package router

import (
	"jukebox-server/controllers"
	middlewareCustom "jukebox-server/middleware"

	"github.com/labstack/echo/v4"
)

func InitRouterSpotify(e *echo.Group) {
	// Users endpoints
	gSpotify := e.Group("/spotify")
	gSpotify.POST("/login", controllers.SpotifyLogin, middlewareCustom.AdminGuard)
	gSpotify.GET("/callback", controllers.Callback)
	// gSpotify.GET("/verify_token", controllers.VerifyToken, middlewareCustom.AuthGuard)
	gSpotify.GET("/sync_playlist", controllers.SyncPlaylist, middlewareCustom.AuthGuard)
	gSpotify.GET("/add_track", controllers.AddTrack, middlewareCustom.AuthGuard)
	gSpotify.GET("/delete_track", controllers.DeleteTrack, middlewareCustom.AuthGuard)
	gSpotify.GET("/current_track", controllers.CurrentTrack, middlewareCustom.AuthGuard)
	gSpotify.POST("/pause_track", controllers.PauseTrack, middlewareCustom.AuthGuard)
	gSpotify.POST("/play_track", controllers.PlayTrack, middlewareCustom.AuthGuard)
	gSpotify.GET("/play_with_option", controllers.PlayWithOptionTrack, middlewareCustom.AuthGuard)
	gSpotify.POST("/skip_to_next", controllers.SkipToNext, middlewareCustom.AuthGuard)
	gSpotify.POST("/skip_to_previous", controllers.SkipToPrevious, middlewareCustom.AuthGuard)
	gSpotify.POST("/set_shuffle", controllers.SetShuffle, middlewareCustom.AuthGuard)
	gSpotify.POST("/set_repeat", controllers.SetRepeat, middlewareCustom.AuthGuard)
	gSpotify.GET("/artist_top_track", controllers.ArtistTopTrack, middlewareCustom.AuthGuard)
	gSpotify.GET("/artist_album", controllers.ArtistAlbum, middlewareCustom.AuthGuard)
	gSpotify.GET("/related_artist", controllers.RelatedArtist, middlewareCustom.AuthGuard)
	gSpotify.GET("/album_info", controllers.AlbumInfo, middlewareCustom.AuthGuard)
}
