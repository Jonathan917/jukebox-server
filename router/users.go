package router

import (
	"jukebox-server/controllers"
	middlewareCustom "jukebox-server/middleware"

	"github.com/labstack/echo/v4"
)

func InitRouteUsers(e *echo.Group) {
	// Users endpoints
	gUser := e.Group("/user")
	gUser.POST("/sign_in", controllers.SignIn)
	gUser.POST("/add_first_user", controllers.AddFirstUser, middlewareCustom.AuthGuard)
	gUser.POST("/change_role_to_admin", controllers.ChangeRoleToAdmin, middlewareCustom.AdminGuard)
	gUser.POST("/change_role_to_member", controllers.ChangeRoleToMember, middlewareCustom.AdminGuard)
	gUser.POST("/remove_from_organization", controllers.RemoveFromOrginazation, middlewareCustom.AdminGuard)
}
