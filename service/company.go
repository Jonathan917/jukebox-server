package service

import (
	CustomContext "jukebox-server/middleware"
	"jukebox-server/model"

	"cloud.google.com/go/firestore"
	"github.com/labstack/echo/v4"
)

func GetCompany(id string, c echo.Context) (model.Company, error) {
	cc := c.(*CustomContext.CustomContext)
	var u model.Company
	snapshot, err := cc.Db.Collection("companies").Doc(id).Get(*cc.Ctx)
	if err != nil {
		return u, err
	}
	if err := snapshot.DataTo(&u); err != nil {
		return u, err
	}
	u.Id = snapshot.Ref.ID
	return u, nil
}

func UpdateCompany(company model.Company, c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	if _, err := cc.Db.Collection("companies").Doc(company.Id).Set(*cc.Ctx, map[string]interface{}{
		"client_id_spotify": company.ClientIdSpotify,
		"password_spotify":  company.PassportSpotify,
		"playlist_id":       company.PlaylistId,
	}, firestore.MergeAll); err != nil {
		return err
	}
	return nil
}
