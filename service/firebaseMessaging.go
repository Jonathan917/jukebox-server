package service

import (
	CustomContext "jukebox-server/middleware"

	"firebase.google.com/go/messaging"

	"github.com/labstack/echo/v4"
)

func SendMessaging(c echo.Context, title, body, companyId string) error {
	cc := c.(*CustomContext.CustomContext)
	var p messaging.Message
	p.Topic = companyId
	p.Notification = &messaging.Notification{
		Title: title, Body: body,
	}
	if _, err := cc.Messaging.Send(*cc.Ctx, &p); err != nil {
		return err
	}
	return nil
}
