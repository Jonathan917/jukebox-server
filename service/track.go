package service

import (
	CustomContext "jukebox-server/middleware"
	"jukebox-server/model"

	"cloud.google.com/go/firestore"
	"github.com/labstack/echo/v4"
	"github.com/zmb3/spotify"
)

func AddTrack(companyId, username string, trackInfo *spotify.FullTrack, c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	var image []map[string]interface{}
	var artists []map[string]interface{}
	for _, j := range trackInfo.Album.Images {
		image = append(image, map[string]interface{}{"url": model.TrackImage{Url: j.URL}.Url})
	}
	for _, artist := range trackInfo.Artists {
		u := model.TrackArtist{Id: artist.ID.String(), Name: artist.Name}
		artists = append(artists, map[string]interface{}{"id": u.Id, "name": u.Name})
	}

	_, err := cc.Db.Collection("companies").Doc(companyId).Collection("tracks").Doc(trackInfo.ID.String()).Set(*cc.Ctx, map[string]interface{}{
		"name":     trackInfo.Name,
		"added_at": firestore.ServerTimestamp,
		"added_by": username,
		"artists":  artists,
		"album": map[string]interface{}{
			"id":    trackInfo.ID.String(),
			"image": image,
			"name":  trackInfo.Album.Name,
		},
		"dislikes":   0,
		"comments":   0,
		"is_deleted": false,
		"likes":      0,
	}, firestore.MergeAll)
	if err != nil {
		return err
	}
	return nil
}

func DeleteTrack(companyId, trackId string, c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	_, err := cc.Db.Collection("companies").Doc(companyId).Collection("tracks").Doc(trackId).Delete(*cc.Ctx)
	if err != nil {
		return err
	}
	return nil
}
