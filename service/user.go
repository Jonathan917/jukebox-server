package service

import (
	CustomContext "jukebox-server/middleware"
	"jukebox-server/model"
	"time"

	"cloud.google.com/go/firestore"
	"github.com/labstack/echo/v4"
)

func GetUser(id string, c echo.Context) (model.User, error) {
	cc := c.(*CustomContext.CustomContext)
	var u model.User
	snapshot, err := cc.Db.Collection("users").Doc(id).Get(*cc.Ctx)
	if err != nil {
		return u, err
	}
	if err := snapshot.DataTo(&u); err != nil {
		return u, err
	}
	u.Id = snapshot.Ref.ID
	return u, nil
}

func UpdateUserRole(id, companyId string, accessLevel int, c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	_, err := cc.Db.Collection("users").Doc(id).Set(*cc.Ctx, map[string]interface{}{
		"companyId": companyId,
		"role": map[string]interface{}{
			"access_level": accessLevel,
		},
	}, firestore.MergeAll)
	if err != nil {
		return err
	}
	return nil
}

func CreateUser(user model.UserSignInDTO, c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	if _, err := cc.Db.Collection("users").Doc(user.Uid).Set(*cc.Ctx, map[string]interface{}{
		"uid":             user.Uid,
		"email":           user.Email,
		"photo_url":       user.PhotoURL,
		"display_name":    user.DisplayName,
		"last_seen":       time.Now(),
		"liked_track":     []string{},
		"disliked_track":  []string{},
		"counter_message": 0,
		"counter_like":    0,
		"liked_event":     []string{},
		"counter_dislike": 0,
		"role": map[string]interface{}{
			"access_level": 0,
		},
		"notification": map[string]interface{}{
			"company": true,
		},
		"created_at": firestore.ServerTimestamp,
		"companyId":  user.CompanyId,
	}, firestore.MergeAll); err != nil {
		return err
	}
	return nil
}
