package service

import (
	CustomContext "jukebox-server/middleware"
	"jukebox-server/model"

	"cloud.google.com/go/firestore"
	"github.com/labstack/echo/v4"
)

func AddEvent(companyId, eventId string, event model.Event, c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	if _, err := cc.Db.Collection("companies").Doc(companyId).Collection("events").Doc(eventId).Set(*cc.Ctx, map[string]interface{}{
		"subject":         event.Subject,
		"type":            event.Type,
		"created_at":      firestore.ServerTimestamp,
		"counter_message": event.CounterMessage,
		"counter_like":    event.CounterLike,
		"image_url":       event.ImageUrl,
		"liker":           event.Liker,
	}, firestore.MergeAll); err != nil {
		return err
	}
	return nil
}
