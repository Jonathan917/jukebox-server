module jukebox-server

go 1.13

require (
	cloud.google.com/go/firestore v1.1.1
	cloud.google.com/go/logging v1.0.0
	cloud.google.com/go/storage v1.6.0
	firebase.google.com/go v3.12.0+incompatible
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/andyfusniak/stackdriver-gae-logrus-plugin v0.1.3
	github.com/asaskevich/govalidator v0.0.0-20200108200545-475eaeb16496
	github.com/cosmtrek/air v1.12.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/fatih/color v1.9.0 // indirect
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/go-openapi/spec v0.19.8 // indirect
	github.com/go-openapi/swag v0.19.9 // indirect
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/go-playground/validator/v10 v10.2.0 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.1.16
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/mailru/easyjson v0.7.1 // indirect
	github.com/mattn/go-colorable v0.1.7 // indirect
	github.com/sirupsen/logrus v1.5.0
	github.com/swaggo/echo-swagger v1.0.0
	github.com/swaggo/swag v1.6.7
	github.com/valyala/fasttemplate v1.2.0 // indirect
	github.com/zmb3/spotify v0.0.0-20200112163645-71a4c67d18db
	golang.org/x/crypto v0.0.0-20200707235045-ab33eee955e0 // indirect
	golang.org/x/net v0.0.0-20200707034311-ab3426394381 // indirect
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	golang.org/x/sys v0.0.0-20200625212154-ddb9806d33ae // indirect
	golang.org/x/text v0.3.3 // indirect
	golang.org/x/tools v0.0.0-20200708003708-134513de8882 // indirect
	google.golang.org/api v0.20.0
	google.golang.org/appengine v1.6.5
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
