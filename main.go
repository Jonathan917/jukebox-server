package main

import (
	"fmt"
	"jukebox-server/controllers"
	middlewareCustom "jukebox-server/middleware"
	"jukebox-server/router"
	utils "jukebox-server/utils"
	"net/http"
	"os"

	"github.com/andyfusniak/stackdriver-gae-logrus-plugin"
	"github.com/go-playground/validator"
	log "github.com/sirupsen/logrus"

	_ "jukebox-server/docs"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	echoSwagger "github.com/swaggo/echo-swagger"
)

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

// @title Swagger Example API
// @version 1.0
// @description jukebox server
// @termsOfService http://swagger.io/terms/

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host
// @BasePath /v1

func main() {
	e := echo.New()

	// env
	e.Logger.Info("the server have been start nornally")
	err := godotenv.Load("./config/" + os.Getenv("APP_ENV") + ".env")
	if err != nil {
		log.Fatalf("Error loading .env file with error %v", err)
	}

	// logger
	if os.Getenv("APP_ENV") == "local" || os.Getenv("APP_ENV") == "dev" {

	}
	if os.Getenv("APP_ENV") == "prod" {
		formatter := stackdriver.GAEStandardFormatter(
			stackdriver.WithProjectID(os.Getenv("GCP_PROJECT_ID")),
		)
		log.SetFormatter(formatter)
	}
	log.SetOutput(os.Stdout)
	log.SetLevel(log.DebugLevel)

	// Middleware
	e.Use(middlewareCustom.InitCustomContext) // Initiation context
	e.Use(middlewareCustom.MiddlewareLogging)
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "${time_rfc3339} method=${method}, uri=${uri}, status=${status}\n",
	}))
	e.Use(middleware.Recover())

	// Error Handler
	e.HTTPErrorHandler = utils.ErrorHandler

	//CORS
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}))

	// Set validator
	e.Validator = &CustomValidator{validator: validator.New()}

	// Swagger
	e.GET("/swagger/*", echoSwagger.WrapHandler)

	// Prefix version
	r := e.Group("/v1")

	// Route => handler
	r.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!\n")
	})
	r.GET("/healthcheck", controllers.HealthCheck)
	router.InitRouteUsers(r)
	router.InitRouterSpotify(r)

	// Start server
	if os.Getenv("APP_ENV") == "local" {
		e.Logger.Fatal(e.Start(":8080"))
	} else if os.Getenv("APP_ENV") == "prod" {
		http.Handle("/", e)
		port := os.Getenv("PORT")
		if port == "" {
			port = "8080"
			log.Printf("Defaulting to port %s", port)
		}
		log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
	}
}
