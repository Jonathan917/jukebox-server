package model

type SpotifyLoginDTO struct {
	SpotifyID       string `json:"spotifyId" validate:"required"`
	SpotifyPassword string `json:"spotifyPassword" validate:"required"`
	// RedirectUrl     string `json:"redirectUrl" validate:"required"`
}

type SpotifyPlaylistDTO struct {
	Playlist string `json:"playlist" validate:"required"`
}

type SpotifyRepeatDTO struct {
	State string `json:"state" validate:"required"`
}

type SpotifyShuffleDTO struct {
	State bool `json:"state"`
}

type SpotifyAddTrackDTO struct {
	Track string `json:"track" validate:"required"`
}

type SpotifyDeleteTrackDTO struct {
	Track string `json:"track" validate:"required"`
}

type SpotifyArtistDTO struct {
	ArtistId string `json:"artistId" validate:"required"`
}

type SpotifyAlbumDTO struct {
	AlbumId string `json:"albumId" validate:"required"`
}

type SpotifyPlayWithOptionDTO struct {
	TrackId string `json:"track_id"`
}
