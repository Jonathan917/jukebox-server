package model

import "time"

type CurrentTrack struct {
	ProgressMs   int               `json:"progress_ms"`
	IsPlaying    bool              `json:"is_playing"`
	DurationMs   int               `json:"duration_ms"`
	ShuffleState bool              `json:"shuffle_state"`
	RepeatState  string            `json:"repeat_state"`
	Album        CurrentTrackAlbum `json:"album"`
	Artists      []TrackArtist     `json:"artists"`
	Id           string            `json:"id"`
	Name         string            `json:"name"`
	AddedAt      time.Time         `json:"added_at"`
}

type CurrentTrackAlbum struct {
	Id    string       `json:"id"`
	Image []TrackImage `json:"image"`
	Name  string       `json:"name"`
}
