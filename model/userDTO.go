package model

type UserSignInDTO struct {
	Uid         string `json:"uid" validate:"required"`
	Email       string `json:"email" validate:"required,email"`
	DisplayName string `json:"displayName" validate:"required"`
	PhotoURL    string `json:"photoURL"`
	CompanyId   string `json:"companyId"`
}

type UserBaseDTO struct {
	Uid       string `json:"uid" validate:"required"`
	CompanyId string `json:"companyId" validate:"required"`
}

type UserFirstDTO struct {
	CompanyId string `json:"companyId"`
}
