package model

type AlbumInfo struct {
	Id         string       `json:"id"`
	ImageUrl   string       `json:"image_url"`
	Tracks     []AlbumTrack `json:"tracks"`
	Name       string       `json:"name"`
	ArtistName string       `json:"artist_name"`
}

type AlbumTrack struct {
	Id          string `json:"id"`
	TrackNumber int    `json:"track_number"`
	Name        string `json:"name"`
}
