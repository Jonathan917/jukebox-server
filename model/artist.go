package model

type RelatedArtist struct {
	Id         string `json:"id"`
	Name       string `json:"name"`
	ImagesUrl  string `json:"images_url"`
	Popularity int    `json:"popularity"`
}
