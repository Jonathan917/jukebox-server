package model

import (
	"time"
)

type Event struct {
	Subject        string        `firestore:"subject"`
	Type           int           `firestore:"type"`
	CreatedAt      time.Time     `firestore:"created_at"`
	CounterMessage int           `firestore:"counter_message"`
	CounterLike    int           `firestore:"counter_like"`
	ImageUrl       string        `firestore:"image_url"`
	Liker          []interface{} `firestore:"liker"`
}
