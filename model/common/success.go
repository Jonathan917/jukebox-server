package common

type Success struct {
	Code    int
	Message string
	Data    interface{}
}
