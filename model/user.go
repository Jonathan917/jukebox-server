package model

import "time"

type User struct {
	CompanyId      string                 `firestore:"companyId"`
	CounterDislike int64                  `firestore:"counter_dislike"`
	CounterLike    int64                  `firestore:"counter_like"`
	CounterMessage int64                  `firestore:"counter_message"`
	CreatedAt      time.Time              `firestore:"created_at"`
	DislikedTrack  []interface{}          `firestore:"disliked_track"`
	DisplayName    string                 `firestore:"display_name"`
	Email          string                 `firestore:"email"`
	LastSeen       time.Time              `firestore:"last_seen"`
	LikedEvent     []interface{}          `firestore:"liked_event"`
	LikedTrack     []interface{}          `firestore:"liked_track"`
	PhotoUrl       string                 `firestore:"photo_url"`
	Role           map[string]interface{} `firestore:"role"`
	Uid            string                 `firestore:"uid"`
	Id             string                 `firestore:"id"`
}
