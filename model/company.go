package model

import "golang.org/x/oauth2"

type Company struct {
	ClientIdSpotify string        `firestore:"client_id_spotify"`
	PassportSpotify string        `firestore:"password_spotify"`
	Name            string        `firestore:"name"`
	PlaylistId      string        `firestore:"playlist_id"`
	Token           *oauth2.Token `firestore:"token"`
	Id              string        `firestore:"id"`
}
