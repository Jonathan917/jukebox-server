package model

import "time"

type Track struct {
	Album   TrackAlbum    `json:"album"`
	Artists []TrackArtist `json:"artists"`
	Id      string        `json:"id"`
	Name    string        `json:"name"`
	AddedAt time.Time     `json:"added_at"`
}

type TrackImage struct {
	Url string `json:"url"`
}

type TrackArtist struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

type TrackAlbum struct {
	Id    string       `json:"id"`
	Image []TrackImage `json:"image"`
	Name  string       `json:"name"`
}
