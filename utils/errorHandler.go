package utils

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
)

func ErrorHandler(err error, c echo.Context) {
	report, ok := err.(*echo.HTTPError)
	if !ok {
		report = echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	report.Message = fmt.Sprintf("http error %d - %v", report.Code, report.Message)
	if report.Code >= 500 {
		Log(c).Error(report.Message)
	} else {
		Log(c).Warn(report.Message)
	}
	c.JSON(report.Code, report)
}
