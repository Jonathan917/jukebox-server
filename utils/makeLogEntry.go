package utils

import (
	"fmt"
	"time"

	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"
)

// logrus
func Log(c echo.Context) *log.Entry {
	contextLogger := log.WithContext(c.Request().Context())
	var uid string = fmt.Sprintf("%v", c.Get("Uid"))
	if c == nil {
		return contextLogger.WithFields(log.Fields{
			"at": time.Now().Format("2006-01-02 15:04:05"),
		})
	}

	return contextLogger.WithFields(log.Fields{
		"at":     time.Now().Format("2006-01-02 15:04:05"),
		"method": c.Request().Method,
		"uri":    c.Request().URL.String(),
		// "ip":     c.Request().RemoteAddr,
		"uid": uid,
	})
}
