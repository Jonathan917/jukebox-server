package controllers

import (
	CustomContext "jukebox-server/middleware"
	"jukebox-server/model"
	"jukebox-server/service"
	"jukebox-server/utils"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
)

func SignIn(c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	u := new(model.UserSignInDTO)
	if err := c.Bind(u); err != nil {
		return err
	}
	if err := c.Validate(u); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	if u.CompanyId != "" {
		_, err := service.GetCompany(u.CompanyId, c)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		if err = cc.Auth.SetCustomUserClaims(*cc.Ctx, cc.Get("Uid").(string), map[string]interface{}{
			"role": map[string]interface{}{"access_level": 0, "company_id": u.CompanyId},
		}); err != nil {
			return err
		}
	}
	if err := service.CreateUser(*u, c); err != nil {
		return err
	}
	if u.CompanyId != "" {
		var inter []interface{}
		event := model.Event{
			Subject:        u.DisplayName + " have joined.",
			Type:           3,
			CreatedAt:      time.Now(),
			CounterMessage: 0,
			CounterLike:    0,
			ImageUrl:       "",
			Liker:          inter,
		}
		if err := service.AddEvent(u.CompanyId, u.Uid, event, c); err != nil {
			return err
		}
	}
	return c.String(http.StatusOK, "")
}

func AddFirstUser(c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	u := new(model.UserFirstDTO)
	if err := c.Bind(u); err != nil {
		return err
	}
	if err := c.Validate(u); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	_, err := service.GetCompany(u.CompanyId, c)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest)
	}
	err = cc.Auth.SetCustomUserClaims(*cc.Ctx, cc.Get("Uid").(string), map[string]interface{}{
		"role": map[string]interface{}{"access_level": 10, "company_id": u.CompanyId},
	})
	if err != nil {
		return err
	}
	if err := service.UpdateUserRole(cc.Get("Uid").(string), u.CompanyId, 10, c); err != nil {
		return err
	}
	return c.String(http.StatusOK, "")
}

func ChangeRoleToAdmin(c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	u := new(model.UserBaseDTO)
	if err := c.Bind(u); err != nil {
		return err
	}
	if err := c.Validate(u); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	err := cc.Auth.SetCustomUserClaims(*cc.Ctx, u.Uid, map[string]interface{}{
		"role": map[string]interface{}{"access_level": 10, "company_id": u.CompanyId},
	})
	if err != nil {
		return err
	}
	if err := service.UpdateUserRole(u.Uid, u.CompanyId, 10, c); err != nil {
		return err
	}
	utils.Log(c).Infof("the user " + u.Uid + " became admin")
	return c.String(http.StatusOK, "")
}

func ChangeRoleToMember(c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	u := new(model.UserBaseDTO)
	if err := c.Bind(u); err != nil {
		return err
	}
	if err := c.Validate(u); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	err := cc.Auth.SetCustomUserClaims(*cc.Ctx, u.Uid, map[string]interface{}{
		"role": map[string]interface{}{"access_level": 5, "company_id": u.CompanyId},
	})
	if err != nil {
		return err
	}
	if err := service.UpdateUserRole(u.Uid, u.CompanyId, 5, c); err != nil {
		return err
	}
	utils.Log(c).Infof("the user " + u.Uid + " became normal member")
	return c.String(http.StatusOK, "")
}

func RemoveFromOrginazation(c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	u := new(model.UserBaseDTO)
	if err := c.Bind(u); err != nil {
		return err
	}
	if err := c.Validate(u); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	err := cc.Auth.SetCustomUserClaims(*cc.Ctx, u.Uid, map[string]interface{}{
		"role": map[string]interface{}{"access_level": 0, "company_id": nil},
	})
	if err != nil {
		return err
	}
	if err := service.UpdateUserRole(u.Uid, u.CompanyId, 0, c); err != nil {
		return err
	}
	utils.Log(c).Infof("the user " + u.Uid + " have been removed from the company " + u.CompanyId)
	return c.String(http.StatusOK, "")
}
