package controllers

import (
	"jukebox-server/utils"
	"net/http"

	"github.com/labstack/echo/v4"
)

// @Summary HealthCheck
// @Description get status of the server
// @Router /healthcheck [get]
func HealthCheck(c echo.Context) error {
	utils.Log(c).Infof("Everything is ok")
	return c.String(http.StatusOK, "Everything is ok")
}
