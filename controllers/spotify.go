package controllers

import (
	"fmt"
	CustomContext "jukebox-server/middleware"
	"jukebox-server/model"
	Model "jukebox-server/model"
	"jukebox-server/model/common"
	"jukebox-server/service"
	"jukebox-server/utils"
	"net/http"
	"time"

	"cloud.google.com/go/firestore"
	"github.com/labstack/echo/v4"
	"github.com/zmb3/spotify"
)

type userCompany struct {
	company Model.Company
	user    Model.User
}

func getUserCompany(c echo.Context) (userCompany, error) {
	cc := c.(*CustomContext.CustomContext)
	var s userCompany
	var uid string = fmt.Sprintf("%v", cc.Get("Uid"))
	user, err := service.GetUser(uid, c)
	if err != nil {
		return s, err
	}
	company, err := service.GetCompany(user.CompanyId, c)
	if err != nil {
		return s, err
	}
	s = userCompany{company, user}
	return s, nil
}

func SpotifyLogin(c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	u := new(model.SpotifyLoginDTO)
	if err := c.Bind(u); err != nil {
		return err
	}
	if err := c.Validate(u); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	companyUser, err := getUserCompany(c)
	if err != nil {
		return err
	}
	companyUser.company.ClientIdSpotify = u.SpotifyID
	companyUser.company.PassportSpotify = u.SpotifyPassword
	if err := service.UpdateCompany(companyUser.company, c); err != nil {
		return err
	}
	cc.SpotifyApi.SetAuthInfo(u.SpotifyID, u.SpotifyPassword)
	url := cc.SpotifyApi.AuthURL(companyUser.company.Id)
	utils.Log(c).Debugf("%s", url)
	return c.JSON(http.StatusOK, map[string]string{"url": url})
}

func Callback(c echo.Context) error {
	state := c.QueryParam("state")
	cc := c.(*CustomContext.CustomContext)
	token, err := cc.SpotifyApi.Token(state, cc.Request())
	if err != nil {
		cc.Db.Collection("companies").Doc(state).Set(*cc.Ctx, map[string]interface{}{
			"token": map[string]string{},
		}, firestore.MergeAll)
		return err
	}
	cc.Db.Collection("companies").Doc(state).Set(*cc.Ctx, map[string]interface{}{
		"token": token,
	}, firestore.MergeAll)
	return c.String(http.StatusOK, "Success creation token")
}

func SyncPlaylist(c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	u := new(model.SpotifyPlaylistDTO)
	if err := c.Bind(u); err != nil {
		return err
	}
	if err := c.Validate(u); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	companyUser, err := getUserCompany(c)
	if err != nil {
		return err
	}
	cc.SpotifyApi.SetAuthInfo(companyUser.company.ClientIdSpotify, companyUser.company.PassportSpotify)
	client := cc.SpotifyApi.NewClient(companyUser.company.Token)
	tracks, err := client.GetPlaylistTracks(spotify.ID(u.Playlist))
	if err != nil {
		return err
	}
	companyUser.company.PlaylistId = u.Playlist
	service.UpdateCompany(companyUser.company, c)
	errMessage := make(chan error)
	done := make(chan error)
	for _, track := range tracks.Tracks {
		go func(track spotify.PlaylistTrack) {
			service.AddTrack(companyUser.company.Id, companyUser.user.DisplayName, &track.Track, c)
			ch := done
			if err != nil {
				errMessage <- err
			}
			select {
			case ch <- err:
				return
			}
		}(track)
	}
	count := 0
	for {
		select {
		case err := <-errMessage:
			return err
		case <-done:
			count++
			if count == len(tracks.Tracks) {
				utils.Log(c).Infof("The playlist %s have been updated", companyUser.company.PlaylistId)
				return c.String(http.StatusOK, "")
			}
		}
	}
}

func AddTrack(c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	u := new(model.SpotifyAddTrackDTO)
	if err := c.Bind(u); err != nil {
		return err
	}
	if err := c.Validate(u); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	companyUser, err := getUserCompany(c)
	if err != nil {
		return err
	}
	cc.SpotifyApi.SetAuthInfo(companyUser.company.ClientIdSpotify, companyUser.company.PassportSpotify)
	client := cc.SpotifyApi.NewClient(companyUser.company.Token)
	if _, err = client.AddTracksToPlaylist(spotify.ID(companyUser.company.PlaylistId), spotify.ID(u.Track)); err != nil {
		return err
	}
	track, err := client.GetTrack(spotify.ID(u.Track))
	if err != nil {
		return err
	}
	if err := service.AddTrack(companyUser.company.Id, companyUser.user.DisplayName, track, c); err != nil {
		return err
	}
	var inter []interface{}
	event := Model.Event{
		Subject:        companyUser.user.DisplayName + " added the track " + track.Name,
		Type:           1,
		CreatedAt:      time.Now(),
		CounterMessage: 0,
		CounterLike:    0,
		ImageUrl:       string(track.Album.URI),
		Liker:          inter,
	}
	if err := service.AddEvent(companyUser.company.Id, track.ID.String(), event, c); err != nil {
		return err
	}
	utils.Log(c).Infof("%s have been added to the playlist %s", track, companyUser.company.PlaylistId)
	return c.String(http.StatusOK, "")
}

func DeleteTrack(c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	u := new(model.SpotifyDeleteTrackDTO)
	if err := c.Bind(u); err != nil {
		return err
	}
	if err := c.Validate(u); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	companyUser, err := getUserCompany(c)
	if err != nil {
		return err
	}
	cc.SpotifyApi.SetAuthInfo(companyUser.company.ClientIdSpotify, companyUser.company.PassportSpotify)
	client := cc.SpotifyApi.NewClient(companyUser.company.Token)
	if _, err = client.RemoveTracksFromPlaylist(spotify.ID(companyUser.company.PlaylistId), spotify.ID(u.Track)); err != nil {
		return err
	}
	if err = service.DeleteTrack(companyUser.company.Id, u.Track, c); err != nil {
		return err
	}
	utils.Log(c).Infof("the track %s have been removed to the playlist %s", u.Track, companyUser.company.PlaylistId)
	return c.String(http.StatusOK, "")
}

func CurrentTrack(c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	companyUser, err := getUserCompany(c)
	if err != nil {
		return err
	}
	cc.SpotifyApi.SetAuthInfo(companyUser.company.ClientIdSpotify, companyUser.company.PassportSpotify)
	client := cc.SpotifyApi.NewClient(companyUser.company.Token)
	currentPlaying, err := client.PlayerCurrentlyPlaying()
	if err != nil {
		return err
	}
	utils.Log(c).Debugf("%s", currentPlaying.Item)
	var ct Model.CurrentTrack
	if currentPlaying.Item != nil {
		playerState, err := client.PlayerState()
		if err != nil {
			return err
		}
		var artists []Model.TrackArtist
		var images []Model.TrackImage
		for _, image := range playerState.Item.Album.Images {
			images = append(images, model.TrackImage{
				Url: image.URL,
			})
		}
		for _, artist := range currentPlaying.Item.Artists {
			artists = append(artists, model.TrackArtist{Id: artist.ID.String(), Name: artist.Name})
		}
		ct = model.CurrentTrack{
			ProgressMs:   currentPlaying.Progress,
			IsPlaying:    currentPlaying.Playing,
			DurationMs:   currentPlaying.Item.Duration,
			ShuffleState: playerState.ShuffleState,
			RepeatState:  playerState.RepeatState,
			Album: model.CurrentTrackAlbum{
				Id:    playerState.Item.Album.ID.String(),
				Name:  playerState.Item.Album.Name,
				Image: images,
			},
			Artists: artists,
			Name:    currentPlaying.Item.Name,
			AddedAt: time.Now(),
			Id:      currentPlaying.Item.ID.String(),
		}
	} else {
		ct.Album.Image = make([]Model.TrackImage, 0)
		ct.Artists = make([]Model.TrackArtist, 0)
	}
	return c.JSON(http.StatusOK, ct)
}

func PauseTrack(c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	companyUser, err := getUserCompany(c)
	if err != nil {
		return err
	}
	cc.SpotifyApi.SetAuthInfo(companyUser.company.ClientIdSpotify, companyUser.company.PassportSpotify)
	client := cc.SpotifyApi.NewClient(companyUser.company.Token)
	err = client.Pause()
	if err != nil {
		return err
	}
	b := companyUser.user.DisplayName + " paused the playlist."
	if err = service.SendMessaging(c, "Pause", b, companyUser.company.Id); err != nil {
		return err
	}
	utils.Log(c).Infof("the playlist %s have been paused", companyUser.company.PlaylistId)
	return c.String(http.StatusOK, "")
}

func PlayTrack(c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	companyUser, err := getUserCompany(c)
	if err != nil {
		return err
	}
	cc.SpotifyApi.SetAuthInfo(companyUser.company.ClientIdSpotify, companyUser.company.PassportSpotify)
	client := cc.SpotifyApi.NewClient(companyUser.company.Token)
	if err = client.Play(); err != nil {
		return err
	}
	b := companyUser.user.DisplayName + " started the playlist."
	if err = service.SendMessaging(c, "Play", b, companyUser.company.Id); err != nil {
		return err
	}
	utils.Log(c).Infof("the playlist %s have been started", companyUser.company.PlaylistId)
	return c.JSON(http.StatusOK, "")
}

func PlayWithOptionTrack(c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	trackID := c.QueryParam("track_id")
	companyUser, err := getUserCompany(c)
	if err != nil {
		return err
	}
	cc.SpotifyApi.SetAuthInfo(companyUser.company.ClientIdSpotify, companyUser.company.PassportSpotify)
	client := cc.SpotifyApi.NewClient(companyUser.company.Token)
	var urlPlaylist spotify.URI = spotify.URI("spotify:playlist:" + companyUser.company.PlaylistId)
	var urlTrack spotify.URI = spotify.URI("spotify:track:" + trackID)
	opt := spotify.PlayOptions{
		PlaybackContext: &urlPlaylist,
	}
	if trackID != "" {
		opt.PlaybackOffset = &spotify.PlaybackOffset{
			URI: urlTrack,
		}
	}
	utils.Log(c).Debugf("%s", opt)
	if err = client.PlayOpt(&opt); err != nil {
		return err
	}
	b := companyUser.user.DisplayName + " started the playlist."
	if err = service.SendMessaging(c, "Play", b, companyUser.company.Id); err != nil {
		return err
	}
	utils.Log(c).Infof("the playlist %s have been started with the track %s", companyUser.company.PlaylistId, trackID)
	return c.String(http.StatusOK, "")
}

func SkipToNext(c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	companyUser, err := getUserCompany(c)
	if err != nil {
		return err
	}
	cc.SpotifyApi.SetAuthInfo(companyUser.company.ClientIdSpotify, companyUser.company.PassportSpotify)
	client := cc.SpotifyApi.NewClient(companyUser.company.Token)
	if err = client.Next(); err != nil {
		return err
	}
	b := companyUser.user.DisplayName + " skipped to next track"
	if err = service.SendMessaging(c, "Skip To Next", b, companyUser.company.Id); err != nil {
		return err
	}
	utils.Log(c).Infof("the playlist %s have been skip to next", companyUser.company.PlaylistId)
	return c.String(http.StatusOK, "")
}

func SkipToPrevious(c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	companyUser, err := getUserCompany(c)
	if err != nil {
		return err
	}
	cc.SpotifyApi.SetAuthInfo(companyUser.company.ClientIdSpotify, companyUser.company.PassportSpotify)
	client := cc.SpotifyApi.NewClient(companyUser.company.Token)
	if err = client.Previous(); err != nil {
		return err
	}
	b := companyUser.user.DisplayName + " skipped to previous track"
	if err = service.SendMessaging(c, "Skip To Previous", b, companyUser.company.Id); err != nil {
		return err
	}
	utils.Log(c).Infof("the playlist %s have been skip to previous", companyUser.company.PlaylistId)
	return c.String(http.StatusOK, "")
}

func SetShuffle(c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	u := new(model.SpotifyShuffleDTO)
	if err := c.Bind(u); err != nil {
		return err
	}
	if err := c.Validate(u); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	companyUser, err := getUserCompany(c)
	if err != nil {
		return err
	}
	cc.SpotifyApi.SetAuthInfo(companyUser.company.ClientIdSpotify, companyUser.company.PassportSpotify)
	client := cc.SpotifyApi.NewClient(companyUser.company.Token)
	if err = client.Shuffle(u.State); err != nil {
		return err
	}
	utils.Log(c).Infof("the playlist %s have been set shuffle to %b", companyUser.company.PlaylistId, u.State)
	return c.String(http.StatusOK, "")
}

func SetRepeat(c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	u := new(model.SpotifyRepeatDTO)
	if err := c.Bind(u); err != nil {
		return err
	}
	if err := c.Validate(u); err != nil {
		return err
	}
	companyUser, err := getUserCompany(c)
	if err != nil {
		return err
	}
	cc.SpotifyApi.SetAuthInfo(companyUser.company.ClientIdSpotify, companyUser.company.PassportSpotify)
	client := cc.SpotifyApi.NewClient(companyUser.company.Token)
	if err = client.Repeat(u.State); err != nil {
		return err
	}
	utils.Log(c).Infof("the playlist %s have been set repeat to %s", companyUser.company.PlaylistId, u.State)
	return c.String(http.StatusOK, "")
}

func ArtistTopTrack(c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	u := new(model.SpotifyArtistDTO)
	if err := c.Bind(u); err != nil {
		return err
	}
	if err := c.Validate(u); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	companyUser, err := getUserCompany(c)
	if err != nil {
		return err
	}
	cc.SpotifyApi.SetAuthInfo(companyUser.company.ClientIdSpotify, companyUser.company.PassportSpotify)
	client := cc.SpotifyApi.NewClient(companyUser.company.Token)
	topTracks, err := client.GetArtistsTopTracks(spotify.ID(u.ArtistId), "JP")
	if err != nil {
		return err
	}
	var formatedTopTracks []model.Track
	for _, track := range topTracks {
		var image []model.TrackImage
		for _, j := range track.Album.Images {
			image = append(image, model.TrackImage{Url: j.URL})
		}
		album := model.TrackAlbum{Id: track.Album.ID.String(), Name: track.Album.Name, Image: image}
		var artists []Model.TrackArtist
		for _, artist := range track.Artists {
			artists = append(artists, Model.TrackArtist{Id: artist.ID.String(), Name: artist.Name})
		}
		formatedTopTracks = append(formatedTopTracks, Model.Track{
			Album:   album,
			Artists: artists,
			Id:      track.ID.String(),
			Name:    track.Name,
			AddedAt: time.Now(),
		})
	}
	if err != nil {
		return err
	}
	utils.Log(c).Infof("Got artist top track")
	return c.JSON(http.StatusOK, common.Success{Code: http.StatusOK, Message: "Artist top track", Data: formatedTopTracks})
}

func ArtistAlbum(c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	u := new(model.SpotifyArtistDTO)
	if err := c.Bind(u); err != nil {
		return err
	}
	if err := c.Validate(u); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	companyUser, err := getUserCompany(c)
	if err != nil {
		return err
	}
	cc.SpotifyApi.SetAuthInfo(companyUser.company.ClientIdSpotify, companyUser.company.PassportSpotify)
	client := cc.SpotifyApi.NewClient(companyUser.company.Token)
	artistAlbums, err := client.GetArtistAlbums(spotify.ID(u.ArtistId))
	if err != nil {
		return err
	}
	var albums []Model.TrackAlbum
	for _, album := range artistAlbums.Albums {
		var imageList []Model.TrackImage
		for _, image := range album.Images {
			imageList = append(imageList, Model.TrackImage{Url: image.URL})
		}
		albums = append(albums, Model.TrackAlbum{
			Id:    album.ID.String(),
			Image: imageList,
			Name:  album.Name,
		})
	}
	utils.Log(c).Infof("Got artist album")
	return c.JSON(http.StatusOK, common.Success{Code: http.StatusOK, Message: "Artist album", Data: albums})
}

func RelatedArtist(c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	u := new(model.SpotifyArtistDTO)
	if err := c.Bind(u); err != nil {
		return err
	}
	if err := c.Validate(u); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	companyUser, err := getUserCompany(c)
	if err != nil {
		return err
	}
	cc.SpotifyApi.SetAuthInfo(companyUser.company.ClientIdSpotify, companyUser.company.PassportSpotify)
	client := cc.SpotifyApi.NewClient(companyUser.company.Token)
	relatedArtists, err := client.GetRelatedArtists(spotify.ID(u.ArtistId))
	if err != nil {
		return err
	}
	var artists []Model.RelatedArtist
	for _, relatedArtist := range relatedArtists {
		artists = append(artists, Model.RelatedArtist{
			Id:         relatedArtist.ID.String(),
			Name:       relatedArtist.Name,
			ImagesUrl:  relatedArtist.Images[0].URL,
			Popularity: relatedArtist.Popularity,
		})
	}
	utils.Log(c).Infof("Got related artist")
	return c.JSON(http.StatusOK, common.Success{Code: http.StatusOK, Message: "Related artist", Data: artists})
}

func AlbumInfo(c echo.Context) error {
	cc := c.(*CustomContext.CustomContext)
	u := new(model.SpotifyAlbumDTO)
	if err := c.Bind(u); err != nil {
		return err
	}
	if err := c.Validate(u); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	companyUser, err := getUserCompany(c)
	cc.SpotifyApi.SetAuthInfo(companyUser.company.ClientIdSpotify, companyUser.company.PassportSpotify)
	client := cc.SpotifyApi.NewClient(companyUser.company.Token)
	album, err := client.GetAlbum(spotify.ID(u.AlbumId))
	if err != nil {
		return err
	}
	var tracks []Model.AlbumTrack
	for _, item := range album.Tracks.Tracks {
		tracks = append(tracks, Model.AlbumTrack{
			Id:          item.ID.String(),
			TrackNumber: item.TrackNumber,
			Name:        item.Name,
		})
	}
	albumInfo := Model.AlbumInfo{
		Id:         album.ID.String(),
		ImageUrl:   album.Images[0].URL,
		Tracks:     tracks,
		Name:       album.Name,
		ArtistName: album.Artists[0].Name,
	}
	if err != nil {
		return err
	}
	utils.Log(c).Infof("Got album info")
	return c.JSON(http.StatusOK, common.Success{Code: http.StatusOK, Message: "Album info", Data: albumInfo})
}
