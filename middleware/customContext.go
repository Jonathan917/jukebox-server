package middleware

import (
	"context"
	"fmt"
	"os"

	"cloud.google.com/go/firestore"
	"cloud.google.com/go/storage"
	firebase "firebase.google.com/go"
	"firebase.google.com/go/auth"
	"firebase.google.com/go/messaging"
	"github.com/labstack/echo/v4"
	"github.com/zmb3/spotify"
	"google.golang.org/api/option"
)

type CustomContext struct {
	echo.Context
	Db         *firestore.Client
	Storage    *storage.BucketHandle
	Auth       *auth.Client
	Messaging  *messaging.Client
	Ctx        *context.Context
	SpotifyApi *spotify.Authenticator
	// ClientLogger *logging.Client
}

func InitCustomContext(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		// context
		ctx := context.Background()

		// Init firebase
		opt := option.WithCredentialsFile("./credential/serviceAccountKey.json")
		app, err := firebase.NewApp(context.Background(), nil, opt)
		if err != nil {
			fmt.Println("error initializing app: %w", err)
		}
		firestore, err := app.Firestore(ctx)
		if err != nil {
			fmt.Println(err)
		}
		storage, err := app.Storage(ctx)
		if err != nil {
			fmt.Println(err)
		}
		bucket, err := storage.DefaultBucket()
		auth, err := app.Auth(ctx)
		if err != nil {
			fmt.Println(err)
		}
		messaging, err := app.Messaging(ctx)
		if err != nil {
			fmt.Println(err)
		}

		// Init spotify
		spotifyWebAPI := spotify.NewAuthenticator(os.Getenv("REDIRECT_URL"), "user-read-private", "user-read-email", "playlist-modify-private", "playlist-modify-public", "user-read-currently-playing", "user-read-playback-state", "user-modify-playback-state", spotify.ScopeUserModifyPlaybackState)

		cc := &CustomContext{c, firestore, bucket, auth, messaging, &ctx, &spotifyWebAPI}
		return next(cc)
	}
}
