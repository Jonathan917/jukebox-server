package middleware

import (
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
)

func AuthGuard(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		cc := c.(*CustomContext)
		var header = c.Request().Header["Authorization"]
		if header != nil {
			idToken := strings.Split(header[0], " ")
			authToken, err := cc.Auth.VerifyIDToken(*cc.Ctx, idToken[1])
			if err != nil {
				return echo.NewHTTPError(http.StatusUnauthorized, "No permission")
			}
			cc.Set("Uid", authToken.UID)
		} else {
			return echo.NewHTTPError(http.StatusUnauthorized, "No permission")
		}
		return next(c)
	}
}

func AdminGuard(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		cc := c.(*CustomContext)
		var header = c.Request().Header["Authorization"]
		if header != nil {
			idToken := strings.Split(header[0], " ")
			authToken, err := cc.Auth.VerifyIDToken(*cc.Ctx, idToken[1])
			if err != nil {
				return echo.NewHTTPError(http.StatusUnauthorized, "No permission")
			}
			claims := authToken.Claims
			if admin, ok := claims["role"]; ok {
				accessLevel, err := admin.(map[string]interface{})["access_level"]
				if !err {
					return echo.NewHTTPError(http.StatusUnauthorized, "No permission")
				}
				if accessLevel.(float64) >= 10 {
					cc.Set("Uid", authToken.UID)
				} else {
					return echo.NewHTTPError(http.StatusUnauthorized, "No permission")
				}
			} else {
				return echo.NewHTTPError(http.StatusUnauthorized, "No permission")
			}
		} else {
			return echo.NewHTTPError(http.StatusUnauthorized, "No permission")
		}
		return next(c)
	}
}
