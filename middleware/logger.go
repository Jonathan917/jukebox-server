package middleware

import (
	utils "jukebox-server/utils"

	"github.com/labstack/echo/v4"
)

func MiddlewareLogging(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		utils.Log(c).Info("incoming request")
		return next(c)
	}
}
